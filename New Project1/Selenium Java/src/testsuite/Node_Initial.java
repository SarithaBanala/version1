package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Node_Initial
*/
public class Node_Initial extends PageObjectBase
{

	public Node_Initial()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test() throws Exception

	{

	S1_Page s1_page_init=PageFactory.initElements(driver, S1_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Node_Initial", "TC_Node_Initial", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify S1 screen");

	testReport.fillTableStep("Step 1", "verify S1 screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Node_Initial","Step_1");

	Reporter.log("Step - 2- Fill F1 form S1 screen");

	testReport.fillTableStep("Step 2", "Fill F1 form S1 screen");

	getScreenshot(driver,Configurations.screenshotLocation , "Node_Initial","Step_2");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_1");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Node_Initial");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
